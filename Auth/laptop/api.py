# Laptop Service
import os
import pymongo
from pymongo import MongoClient
from flask import Flask, jsonify, request, render_template, redirect, url_for, flash, session
from flask_restful import Resource, Api
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from password import hash_password, verify_password
from wtforms.validators import DataRequired

 

# Instantiate the app
app = Flask(__name__)
api = Api(app)
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
db_1 = client.users
SECRET_KEY = "It's a secret to everybody"
DEBUG = True
app.config.from_object(__name__)
    

#@login_manager.user_loader
#def load_user(id):
    #return Users.query.get(int(id))



class LoginForm(Form):
    username = StringField('username', [validators.length(min=1,max=50)])
    password = PasswordField('password', validators=[DataRequired()])

class RegisterForm(Form):
    username = StringField('username', [validators.length(min=1, max=50)])
    password = PasswordField('password', validators=[DataRequired()])




@app.route("/register", methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        username = form.username.data
        password = form.password.data
        hashpassword = hash_password(password)
        user_item = {
            'username' : username,
            'password' : hashpassword,
        }
        db_1.users.insert(user_item)
        flash("You register successfu")
        return render_template('index.html',form=form)
    return render_template('register.html', form=form)

@app.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm(request.form)

    if request.method == 'POST' and form.validate():
        username = form.username.data
        password = form.password.data
        if username == db_1.users.find_one({'username':username}):
            if verify_password(password, {users["password"]}):
                return render_template('index.html', form = form)
        flash('invalid password of username')
    return render_template('login.html', form = form)

class listAll(Resource):
    def get(self):
        output = []
        for i in db.tododb.find():
            output.append({'open' : i['open'], 'close' : i['close']})

        return jsonify({'result' : output})
class listOpenOnly(Resource):
    def get(self):
        output = []
        for i in db.tododb.find():
            output.append({'open' : i['open']})
        return jsonify({'result': output})
class listCloseOnly(Resource):
    def get(self):
        output = []
        for i in db.tododb.find():
            output.append({'close': i['close']})
        return jsonify({'result': output})
class listAllFormat(Resource):
    def get(self, format):
        if format == 'csv':
            output_1 = ""
            for i in db.tododb.find():
                output_1 = 'Open: ' + i['open'] + ',' + 'Close: ' + i['close'] + ',' + output_1
            return output_1
        elif format == 'json':
            output_2 = []
            for i in db.tododb.find():
                output_2.append({'open' : i['open'], 'close': i['close']})
            return jsonify({'result' : output_2})
class listOpenOnlyFormat(Resource):
    def get(self, format):
        n = request.args.get('top', type = int)
        if n != None:
            if format == 'csv':
                return listOpenTopcsv(n)
            elif format == 'json':
                return listOpenTopjson(n) 
        if format == 'csv':
            output_1 = ""
            for i in db.tododb.find():
                output_1 = i['open'] + ',' + output_1
            return output_1
        elif format == 'json':
            output = []
            for i in db.tododb.find():
                output.append({'open' : i['open']})
            return jsonify({'result' : output})

class listCloseOnlyFormat(Resource):
    def get(self, format):
        n = request.args.get('top', type = int)
        if n != None:
            if format == 'csv':
                return listCloseTopcsv(n)
            elif format == 'json':
                return listCloseTopjson(n) 

        if format == 'csv':
            output_1 = ""
            for i in db.tododb.find():
                output_1 = i['close'] + ',' + output_1
            return output_1
        elif format == 'json':
            output_2 = []
            for i in db.tododb.find():
                output_2.append({'close' : i['close']})
            return jsonify({'result' : output_2})

def listOpenTopjson(n):
    output = []
    if n != 0:
        for i in db.tododb.find().sort([('open', 1)]).limit(n):
            output.append({'oepn':i['open']})
        return jsonify({'result' : output})
def listCloseTopjson(n):
    output = []
    if n != 0:
        for i in db.tododb.find().sort([('close',1)]).limit(n):
            output.append({'close':i['close']})
        return jsonify({'result' : output})
def listOpenTopcsv(n):
    if n !=0:
        output = ""
        for i in db.tododb.find().sort([('open',1)]).limit(n):
            output = i['open'] + ',' + output
        return output

def listCloseTopcsv(n):
    if n !=0:
        output = ""
        for i in db.tododb.find().sort([('close',1)]).limit(n):
            output = i['close'] + ',' + output
        return output
    





# Create routes
# Another way, without decorators
api.add_resource(listAll, '/listAll')
api.add_resource(listAllFormat, '/listAll/<format>')
api.add_resource(listOpenOnly, '/listOpenOnly')
api.add_resource(listOpenOnlyFormat, '/listOpenOnly/<format>')
api.add_resource(listCloseOnly, '/listCloseOnly')
api.add_resource(listCloseOnlyFormat, '/listCloseOnly/<format>')



# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
